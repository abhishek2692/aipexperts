@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Add New Services</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('services.index') }}"> Back</a>
            </div>
        </div>
    </div>


    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


    <form action="{{ route('services.store') }}" method="POST">
        
        <input type="hidden" name="_method" value="PUT">
        {{ csrf_field() }}
        <div class="row">
		    <div class="col-sm-4">
		        <div class="form-group">
		            <strong>Title:</strong>
		            <input type="text" name="name" class="form-control" placeholder="Title" required>
		        </div>
		    </div>
		    <div class="col-sm-4">
		        <div class="form-group">
		            <strong>description:</strong>
		            <textarea class="form-control" style="height:150px" name="description" placeholder="Description"></textarea>
		        </div>
		    </div>
            <div class="col-sm-4">
            <div class="form-group">
                <label for="title">Select Category:</label>
                <select name="category_id" class="form-control" style="width:350px" required>
                    <option value="">--- Select Category ---</option>
                    @foreach ($categories as $key => $value)
                        <option value="{{ $key }}">{{ $value }}</option>
                    @endforeach
                </select>
            </div>
            </div>
            <div class="col-sm-4">
            <div class="form-group">
                <select name="subcategory_id" class="form-control" style="width:350px" required>
                </select>
            </div>
            </div>
        </div>
        <div class="row">    
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="title">Select start date:</label>
                    <div class='input-group date' id='datetimepicker3'>
                        <input type='text' autocomplete="off" class="form-control datepicker" name="activation_start" id="datepicker_start" />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-time"></span>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="title">Select end date:</label>
                        <input type='text' autocomplete="off" class="form-control datepicker" name="activation_end" id="datepicker_end" />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-time"></span>
                        </span>
                </div>
            </div>
		</div>
        <div class="col-sm-4">
                    <button type="submit" class="btn btn-primary">Submit</button>
            </div>
		


    </form>
    
    <script type="text/javascript">
    $(document).ready(function() {
        $('select[name="category_id"]').on('change', function() {

            var categoryID = $(this).val();
            if(categoryID) {
                $.ajax({
                    url: 'subcategory/'+categoryID,
                    type: "GET",
                    dataType: "json",
                    success:function(data) {

                        
                        $('select[name="subcategory_id"]').empty();
                        $.each(data, function(key, value) {
                            $('select[name="subcategory_id"]').append('<option value="'+ key +'">'+ value +'</option>');
                        });


                    }
                });
            }else{
                $('select[name="subcategory_id"]').empty();
            }
        });
    });
    $(function () {
            $("#datepicker_start").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true });
        });
    $(function () {
            $("#datepicker_end").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true });
        });
    </script>


@endsection
