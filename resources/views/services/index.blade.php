@extends('layouts.app')


@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Services</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('services.create') }}"> Create New Service</a>
            </div>
        </div>
    </div>


    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <br>
    <form action="{{ url('/services') }}" method="GET">
      <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <input type="text" name="name" placeholder="Search string"> 
                </div>
            </div>

        <div class="col-sm-4">
        <div class="form-group">
        <select name="category_id" class="form-control" style="width:350px">
            <option value="">--- Select Category ---</option>
            @foreach ($categories as $key => $value)
                <option value="{{ $key }}">{{ $value }}</option>
            @endforeach
        </select>
        </div>
        </div>
        
        <div class="col-sm-4">
        <div class="form-group">
            <select name="subcategory_id" class="form-control" style="width:350px">
            </select>
        </div>
        </div>
    </div>
    <div class="row">
        
        <div class="col-sm-4">
            <div class="form-group">
            <label for="title">Start Date:</label>
            <input autocomplete="off" type='text' class="form-control datepicker" name="activation_start" id="datepicker_start" />
            </div>
        </div>                            

        <div class="col-sm-4">
            <div class="form-group">
            <label for="title">End Date:</label>
            <input autocomplete="off" type='text' class="form-control datepicker" name="activation_end" id="datepicker_end" />
            </div>
        </div>                
    
    </div>
                
    
    <input  type="submit" name="submit" value="Search"> 
    </form>
    <table class="table table-bordered" style="border: 1">
        <tr>
            <th>No</th>
            <th>Title</th>
            <th>Description</th>
            <th>Category</th>
            <th>Sub category</th>
        </tr>

	    @foreach ($services as $service)
        
	    <tr>
	        <td>{{ ++$i }}</td>
	        <td>{{ $service->name }}</td>
	        <td>{{ $service->description }}</td>
            <td>{{ $service['categories']['name'] }}</td>
            <td>{{ $service['subcategories']['name'] }}</td>
	    </tr>
	    @endforeach
           {{ $services->links() }}
 
    </table>

    <script type="text/javascript">
    $(document).ready(function() {
            $('select[name="category_id"]').on('change', function() {

                var categoryID = $(this).val();
                if(categoryID) {
                    $.ajax({
                        url: 'services/subcategory/'+categoryID,
                        type: "GET",
                        dataType: "json",
                        success:function(data) {

                            
                            $('select[name="subcategory_id"]').empty();
                            $.each(data, function(key, value) {
                                $('select[name="subcategory_id"]').append('<option value="'+ key +'">'+ value +'</option>');
                            });


                        }
                    });
                }else{
                    $('select[name="subcategory_id"]').empty();
                }
            });
        });
       
    $(function () {
            $("#datepicker_start").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true });
        });
    $(function () {
            $("#datepicker_end").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true });
        });  
    
    </script>

@endsection
