<?php


namespace App;


use Illuminate\Database\Eloquent\Model;


class Services extends Model
{
    /**
     * The attributes that are mass assignable.
     *	
     * @var array
     */
    protected $table = 'services';

    protected $fillable = [
        'name', 'description','category_id','subcategory_id','status','activation_start_date','activation_end_date'
    ];

    public function categories()
    {
        return $this->belongsTo('App\Categories','category_id','id');
    }

    public function subCategories()
	{
	   return $this->belongsTo('App\SubCategories','subcategory_id','id');
	}
}
