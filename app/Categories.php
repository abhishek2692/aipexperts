<?php


namespace App;


use Illuminate\Database\Eloquent\Model;


class Categories extends Model
{
    /**
     * The attributes that are mass assignable.
     *	
     * @var array
     */
    protected $table = 'categories';

    protected $fillable = [
        'name', 'status'
    ];

    public function services()
	{
	return $this->hasMany('App\Services','id','category_id');
	}
	
	public function subCategories()
	{
	return $this->hasMany('App\SubCategories','id','category_id');
	}

}


