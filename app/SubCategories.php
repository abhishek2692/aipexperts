<?php


namespace App;


use Illuminate\Database\Eloquent\Model;


class SubCategories extends Model
{
    /**
     * The attributes that are mass assignable.
     *  
     * @var array
     */
    protected $table = 'sub_categories';

    protected $fillable = [
        'name', 'status','category_id'
    ];

    public function services()
    {
    return $this->hasMany('App\Services','subcategory_id','id');
    }
    
    public function categories()
    {
    return $this->belongsTo('App\Categories','id','category_id');
    }

}


