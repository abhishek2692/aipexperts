<?php


namespace App\Http\Controllers;


use App\Services;
use App\Categories;
use App\SubCategories;
use Illuminate\Http\Request;
//use Datatables;
use Auth;

class ServiceController extends Controller
{ 
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
                 
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //$services = Services::latest()->paginate(5);
        
        $services = Services::with('categories','subCategories')->where('status',1);
        if (isset ( $request->name )) {
            $services->where ( 'name', 'LIKE', '%' . $request->name . '%' );
        }
        if (isset ( $request->category_id )) {
            $services->where ( 'category_id',$request->category_id );
        }
        if (isset ( $request->subcategory_id )) {
            $services->where ( 'subcategory_id',$request->subcategory_id );
        }

        if (isset($request->activation_start) && !empty ( $request->activation_start ) && !empty ( $request->activation_end ) && isset($request->activation_end) ) {
            
            $startDate=$request->activation_start;
            $endDate=$request->activation_end;
            $services->where(function($q) use($startDate, $endDate) {
                $q->whereBetween('activation_start_date',[$startDate,$endDate]);
                $q->orWhereBetween('activation_end_date',[$startDate,$endDate]);
            });
        }

        $categories = Categories::select('id','name')->get()->pluck('name','id');
        $services=$services->paginate(5);
        
        return view('services.index',compact('services','categories'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $categories = Categories::select('id','name')->get()->pluck('name','id');
        return view('services.create',compact('categories'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        request()->validate([
            'title' => 'required',
            'description' => 'required',
            'category_id' => 'required',
            'subcategory_id' => 'required',
            'activation_start' => 'required',
            'activation_end' => 'required'
        ]);

        $input=$request->all();
        
        Services::create($input);


        return redirect()->route('services.index')
                        ->with('success','Service created successfully.');
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Services $service)
    {
        return view('services.show',compact('service'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Services $service)
    {
        return view('services.edit',compact('service'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Services $service)
    {
         request()->validate([
            'title' => 'required',
            'description' => 'required',
            'category_id' => 'required',
            'subcategory_id' => 'required',
            'activation_start' => 'required',
            'activation_end' => 'required'
        ]);


        $service->update($request->all());


        return redirect()->route('services.index')
                        ->with('success','service updated successfully');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Services $service)
    {
        $service->delete();


        return redirect()->route('services.index')
                        ->with('success','service deleted successfully');
    }

    public function SubcategoryAjax($id)
    {
        $sub_categories = SubCategories::where("category_id",$id)->get()->pluck('name','id');
        return json_encode($sub_categories);
    }
}
